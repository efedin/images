= postgres-exporter

Custom build based on quay.io/testing-farm/postgres_exporter:latest, adds custom queries - see `queries.yaml` to reveal metrics of queries and tables.

See https://github.com/prometheus-community/postgres_exporter/blob/master/queries.yaml for original source.
